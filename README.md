**Lyes FFEZOUI 21965519
Mohamed Yanis SADAOUI 21986699
Binome 18**

**Étape 1 :** Exécutez le script de la création des tables dans tables.sql.

**Étape 2 :** Remplir les tables avec copy.sql, en modifiant le chemin absolu vers /CSV (exemple /home/lyes/Documents/projet/CSV/studio.csv) depuis les fichiers csv disponibles dans le répertoire CSV.

**Étape 3 :** Exécutez les requêtes dans req.sql, chacun requête possède une définition contenue dans un commentaire la précedant.

Données publiques : https://corgis-edu.github.io/corgis/csv/video_games/
