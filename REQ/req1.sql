/* requête qui porte sur trois tables : Quelle sont les jeux vidéo developé par le studio EA et qui sont de la categorie Sport ? */
SELECT j.nom  
FROM Jeu AS j, Studio AS s, Categorie AS c 
WHERE j.idstudio = s.id AND j.idcategorie = c.id AND s.nom = 'EA' AND c.nomcategorie = 'Sport';