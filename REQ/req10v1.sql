/* Gestion des NULL avec une simple jointure :
 Les noms d'entreprises qui ont développé des jeux et construit des consoles */
SELECT DISTINCT s.nom 
FROM Plateforme AS p, Studio AS s 
WHERE p.marque = s.nom
;