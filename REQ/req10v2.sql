/* Gestion des NULL avec INTERSECT :
 Les noms d'entreprises qui ont développé des jeux et construit des consoles */
(SELECT p.marque FROM Plateforme as p) 
INTERSECT ALL 
(SELECT s.nom FROM Studio AS s)
;