/* Donnez la liste des 10 jeux les plus vendus avec le nombre de commandes par jeux */
SELECT j.nom, jec.nombre_commandes 
FROM jeu AS j, (SELECT jc.idjeu, COUNT(jc.idjeu) AS nombre_commandes 
                FROM jeu_commande AS jc 
                GROUP BY jc.idjeu) AS jec 
WHERE jec.idjeu = j.id 
ORDER BY jec.nombre_commandes DESC LIMIT 10;