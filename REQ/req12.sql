/* Les versions de jeux dont les prix évoluent mais ne diminuent jamais */
SELECT DISTINCT j.id AS idjeu, j.nom AS nom_jeu, v.idplateform , p.nomplatform
FROM Jeu AS j, version_jeu as v, plateforme as p
WHERE (v.idjeu, v.idplateform) IN
     (SELECT idjeu, idplateform 
     FROM Prix 
     GROUP BY (idjeu, idplateform )
     HAVING COUNT(idjeu)>1) AND j.id = v.idjeu AND p.id = v.idplateform
EXCEPT 
SELECT DISTINCT p1.idjeu, j.nom, p.id, p.nomplatform
FROM Plateforme as p, Jeu AS j, prix AS p1 JOIN prix AS p2 ON  (p1.idjeu = p2.idjeu 
     AND p2.idplateform =p1.idplateform)
WHERE p1.date_insertion < p2.date_insertion AND (p1.prix > p2.prix ) AND p1.idjeu = j.id 
     AND p.id = p1.idplateform
;