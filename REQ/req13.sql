/*  Donnez pour chaque jeux, la moyenne des notes qu'il a recu */
SELECT j.nom,CAST( AVG(jm.note) AS DECIMAL(10,2)) 
FROM (SELECT v.idjeu AS idjeu , a.note AS note 
      FROM Jeu AS j, version_jeu AS v, avis AS a 
      WHERE v.idjeu = a.idjeu AND a.idplateform = v.idplateform) AS jm 
      JOIN jeu AS j ON j.id = jm.idjeu 
GROUP BY j.id
;