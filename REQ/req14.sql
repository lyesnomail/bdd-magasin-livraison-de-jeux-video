/* Donnez  le nombre et la moyenne d'âge des clients par catégorie de jeux */
SELECT c.nomcategorie, COUNT(cl.id) AS nombre_jeu, CAST(AVG(cl.age) AS DECIMAL(10,2)) AS moyenne_age
FROM  Jeu AS j JOIN Categorie AS c ON (c.id = j.idcategorie) 
    JOIN Jeu_commande AS jc ON ( j.id = jc.idjeu ) JOIN commande AS com 
    ON (jc.idcommande = com.id) JOIN Compte_client AS cl ON (com.idclient = cl.id)
GROUP BY c.id
;