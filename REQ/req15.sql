/* Donnez pour chaque studio leur premier et dernier jeu sortie ? */
WITH lastfirstgame(id, datemin, datemax ) AS (
    SELECT s.id, MIN(v.date_de_sortie) AS datemin, MAX(v.date_de_sortie) AS datemax
    FROM Jeu as j, Studio AS s, version_jeu AS v
    WHERE j.idstudio = s.id AND j.id = v.idjeu
    GROUP BY s.id,s.nom
)
SELECT s.nom AS nom_studio, j1.nom AS nom_premier_jeu, v1.date_de_sortie AS 
    date_de_sortie_premier_jeu, j2.nom AS nom_dernier_jeu , v2.date_de_sortie AS date_de_sortie_dernier_jeu
FROM Studio AS s, Jeu AS j1, Jeu AS j2, version_jeu AS v1, version_jeu AS v2, lastfirstgame AS l
WHERE l.id = s.id AND l.datemin = v1.date_de_sortie AND l.datemax = v2.date_de_sortie
      AND s.id = j1.idstudio AND s.id = j2.idstudio AND j1.id = v1.idjeu AND j2.id = v2.idjeu
;