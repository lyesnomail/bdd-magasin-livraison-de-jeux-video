/* Donnez pour chaque client, les jeux qu'il a le mieux notés  */
WITH maxnote (idcomptecli, notemax) AS (
SELECT a.idcomptecli, max(a.note) AS notemax 
FROM Avis AS a
GROUP BY a.idcomptecli)
SELECT c.id,c.prenom,c.nom, j.nom, av.note
FROM Jeu AS j, Compte_client AS c, maxnote AS a, avis AS av
WHERE j.id = av.idjeu AND  av.note = a.notemax AND av.idcomptecli = a.idcomptecli 
      AND av.idcomptecli = c.id
ORDER BY c.id
;