/* Donnez pour chaque plateforme, le nombre et prix moyen  de ses jeux 
( par rapport à la date d'ajourd'hui ) */
WITH mod(idjeu, idplateform, dateinsert) AS (
  SELECT p.idjeu, p.idplateform, MAX(p.date_insertion) AS dateinsert
  FROM prix AS p 
  WHERE current_date >p.date_insertion
  GROUP BY p.idjeu, p.idplateform ORDER BY p.idplateform, p.idjeu
)

SELECT pl.nomplatform, COUNT(DISTINCT pri.idjeu) AS nombre_de_jeux, 
        CAST( AVG (pri.prix) AS DECIMAL(10,2))  AS prix_moyen_des_jeux
FROM plateforme AS pl, prix AS pri, mod
WHERE  mod.dateinsert = pri.date_insertion AND mod.idjeu = pri.idjeu 
AND mod.idplateform = pri.idplateform AND mod.idplateform = pl.id
GROUP BY pl.nomplatform, pri.idplateform
;