/* Donnez pour chaque commande, la somme totale déboursé pour celle-ci 
(ainsi que le client qui lui est associé) */
WITH dernier_prix_commande (idjeu, idplateform, lastdate) AS 
(SELECT p.idjeu, P.idplateform, MAX(p.date_insertion) AS lastdate
FROM commande AS c, jeu_commande AS jc, prix AS p 
where c.dateinsertion > p.date_insertion AND p.idjeu = jc.idjeu 
      AND p.idplateform = jc.idplateform AND jc.idcommande = c.id
GROUP BY (p.idjeu, p.idplateform)
ORDER BY (p.idplateform,p.idjeu))

SELECT c.id AS id_commande, cl.nom,cl.prenom,SUM(pr.prix) AS somme FROM commande AS c, 
      jeu_commande AS jc, dernier_prix_commande AS p , prix AS pr, compte_client AS cl
WHERE p.idjeu = jc.idjeu AND p.idplateform = jc.idplateform AND jc.idcommande = c.id 
      AND pr.idjeu = p.idjeu AND pr.idplateform=p.idplateform 
      AND pr.date_insertion=p.lastdate AND cl.id = c.idclient
GROUP BY c.id,cl.id
;