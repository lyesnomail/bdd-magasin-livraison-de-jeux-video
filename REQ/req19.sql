/* Donnez pour chaque version de jeu, la difference de prix en pourcentage
 entre chaque nouvelle insertion/modification de prix dans le temps */
WITH datecoupleprix(idjeu, idplateform, premdate, dexdate ) AS 
        (SELECT p1.idjeu, p1.idplateform, p1.date_insertion AS premdate,
                 MIN(p2.date_insertion) AS dexdate
        FROM prix AS p1, prix AS p2 WHERE p1.idjeu = p2.idjeu AND p1.idplateform = p2.idplateform 
        AND p1.date_insertion < p2.date_insertion 
        GROUP BY (p1.idjeu, p1.idplateform, p1.date_insertion) 
        ORDER BY (p1.idjeu, p1.idplateform, p1.date_insertion))
SELECT j.nom, pl.nomplatform, dp.premdate AS premiere_date, dp.dexdate AS deuxieme_date, p1.prix AS prix1,
         p2.prix AS prix2, CONCAT (((p1.prix-p2.prix)*100/p1.prix), '%') as pourcentage_diff
FROM datecoupleprix AS dp,jeu AS j, prix AS p1, prix AS p2, plateforme AS pl
WHERE dp.idjeu = p1.idjeu AND dp.idjeu = p2.idjeu AND dp.idplateform = p1.idplateform 
      AND dp.idplateform = p2.idplateform AND p1.date_insertion = dp.premdate 
      AND p2.date_insertion = dp.dexdate AND j.id = dp.idjeu AND pl.id = dp.idplateform
;