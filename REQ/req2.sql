/* Auto jointure :  pour chaque client, donnez un autre client qui a le même nom que lui*/
SELECT c1.nom, c1.prenom, c2.nom, c2.prenom 
FROM Compte_client AS c1, Compte_client AS c2 
WHERE c1.id != c2.id and c1.nom = c2.nom;