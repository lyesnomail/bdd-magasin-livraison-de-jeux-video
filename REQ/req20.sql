/* Donnez pour chaque mois d'activité, le revenu (mensuel) 
en calculant la somme des entrés - coups */
WITH moisnum(mois) AS(
SELECT DISTINCT EXTRACT(MONTH FROM c.dateinsertion) AS mois
FROM commande AS c),

dernieredate (idjeu,idplateform,dateprix) AS(
SELECT pr.idjeu, pr.idplateform, MAX(pr.date_insertion) AS dateprix
FROM Jeu_commande AS jc, prix AS pr, commande AS c
WHERE jc.idcommande = c.id AND c.dateinsertion >= pr.date_insertion 
AND jc.idjeu = pr.idjeu AND jc.idplateform = pr.idplateform
GROUP BY pr.idjeu, pr.idplateform)

SELECT m.mois, SUM(pr.prix - v.prix_acquisition) AS revenu
FROM prix AS pr, moisnum AS m, dernieredate AS dt , version_jeu AS v, 
     commande AS com, jeu_commande AS jc1
WHERE dt.idjeu = pr.idjeu AND dt.idplateform = pr.idplateform 
      AND dt.dateprix = pr.date_insertion AND v.idjeu = dt.idjeu 
      AND v.idplateform = dt.idplateform AND jc1.idjeu = dt.idjeu 
      AND jc1.idplateform = dt.idplateform AND jc1.idcommande = com.id 
      AND EXTRACT(MONTH FROM com.dateinsertion) = m.mois
GROUP BY m.mois
;