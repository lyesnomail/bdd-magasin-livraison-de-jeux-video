/* Sous requête correlé : Pour chaque jeu dont le prix a été supérieur à 80 euros a un moment, donnez son nom et sa catégorie */
SELECT DISTINCT j.nom,c.nomcategorie 
FROM Jeu AS j, Categorie as c  
WHERE j.id IN 
    (SELECT v.idjeu  
    FROM Version_jeu as v, Prix as pr 
    WHERE j.id = v.idjeu AND pr.idplateform = v.idplateform AND pr.idjeu = v.idjeu AND pr.prix >= 80 ) 
          AND j.idcategorie = c.id;