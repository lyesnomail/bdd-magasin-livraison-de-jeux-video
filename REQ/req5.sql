/* Sous requette dans le WHERE :
 Donnez les versions de jeux  qui n'ont jamais était commandé 
 (une version est la combinaison d'un jeu et de la plate-forme sur laquelle il tourne)*/
SELECT j.nom, p.nomplatform
FROM Jeu AS j, version_jeu AS v, plateforme AS p
WHERE (v.idjeu, v.idplateform) NOT IN ( SELECT DISTINCT jc.idjeu, jc.idplateform FROM jeu_commande AS jc ) 
AND v.idjeu = j.id AND v.idplateform = p.id
ORDER BY p.nomplatform;