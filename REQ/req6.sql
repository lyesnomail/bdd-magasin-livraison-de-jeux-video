/* Deux agrégats nécessitant GROUP BY et HAVING : Donnez pour chaque studio dont l'âge de restriction maximale (de leurs jeux) est strictement 
inférieur à 10 ans, le nombre de jeux qu'ils ont développés */
SELECT s.nom, COUNT(j.id) AS nombre 
FROM Jeu AS j, Studio AS s 
WHERE j.idstudio=s.id  
GROUP BY s.id 
HAVING MAX(j.age_restriction)<10 ;