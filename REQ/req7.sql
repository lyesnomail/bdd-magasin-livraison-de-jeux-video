/* Requete impliquant le calcul de deux agrégats :  
Donnez pour chaque catégorie de jeu, la moyenne des prix maximum insérés dans le temps */
SELECT c.nomcategorie,cast( AVG(Maxp.maxprix) AS DECIMAL(10,3))   as moyenne_des_prix_max
FROM Jeu as j, Categorie as c,
    (SELECT p.idjeu, MAX(p.prix) as maxprix 
    FROM prix as p 
    GROUP BY (p.idjeu) )  
    as Maxp 
WHERE j.id = Maxp.idjeu AND j.idcategorie = c.id
GROUP BY c.id
;