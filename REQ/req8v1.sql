/* Condition de totalité  avec requête corrélée : Les jeux sortie sur toute les plateformes */
SELECT  distinct j.nom  FROM Jeu AS j
WHERE
   NOT EXISTS 
   ( Select * 
     FROM Plateforme as p 
     WHERE p.id NOT IN ( SELECT v.idplateform FROM Version_jeu as v WHERE j.id = v.idjeu  )) ;