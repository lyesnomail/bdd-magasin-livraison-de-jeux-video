/* Condition de totalité  avec agrégation : Les jeux sortie sur toute les plateformes */
SELECT v.idjeu,j.nom 
FROM Jeu AS j, Version_jeu AS v
WHERE  v.idjeu = j.id  
GROUP BY v.idjeu, j.nom
HAVING COUNT(DISTINCT v.idplateform ) = (SELECT COUNT(p1.id) FROM Plateforme AS p1)
;