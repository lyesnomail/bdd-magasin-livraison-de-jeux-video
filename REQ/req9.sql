/* Jointure externe en LEFT JOIN : 
Donnez pour chaque client (nom, prénom et id) les commandes qu'ils ont passé (idcommande et date d'insertion, et NULL si le client na rien commandé avec son compte)  */
SELECT c.nom, c.prenom, c.id as idclient_table_client, com.idclient as idclient_table_commande, com.id as idcommande, com.dateinsertion as date_insertion_de_commande
FROM Compte_client AS c LEFT JOIN Commande as com ON c.id = com.idclient 
ORDER BY c.nom, c.prenom
;