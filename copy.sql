
COPY Compte_client
FROM '/CSV/client.csv'
DELIMITER ',' CSV HEADER;

COPY Studio
FROM '/CSV/studio.csv'
DELIMITER ',' CSV HEADER;

COPY Categorie
FROM '/CSV/categorie.csv'
DELIMITER ',' CSV HEADER;

COPY Plateforme
FROM '/CSV/plateforme.csv'
DELIMITER ',' CSV HEADER;

COPY Jeu
FROM '/CSV/jeux.csv'
DELIMITER ',' CSV HEADER;

COPY Version_jeu
FROM '/CSV/version.csv'
DELIMITER ',' CSV HEADER;

COPY Prix
FROM '/CSV/prixhistorique.csv'
DELIMITER ',' CSV HEADER; 

COPY Commande
FROM '/CSV/commande.csv'
DELIMITER ',' CSV HEADER;

COPY Avis
FROM '/CSV/avis.csv'
DELIMITER ',' CSV HEADER;

COPY Remboursement
FROM '/CSV/remboursement.csv'
DELIMITER ',' CSV HEADER;

COPY Jeu_commande
FROM '/CSV/jeu_commande.csv'
DELIMITER ',' CSV HEADER;

COPY Jeu_en_livraison
FROM '/CSV/jeu_en_livraison.csv'
DELIMITER ',' CSV HEADER;

COPY Jeu_en_preparation
FROM '/CSV/jeu_en_preparation.csv'
DELIMITER ',' CSV HEADER;

COPY Jeu_en_attente
FROM '/CSV/jeu_en_attente.csv'
DELIMITER ',' CSV HEADER;

COPY Jeu_livré
FROM '/CSV/jeu_livré.csv'
DELIMITER ',' CSV HEADER;

COPY Panier
FROM '/CSV/panier.csv'
DELIMITER ',' CSV HEADER;
