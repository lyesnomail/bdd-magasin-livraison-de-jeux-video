/* req 1 : requête qui porte sur trois tables : Quelle sont les jeux vidéo developé 
par le studio EA et qui sont de la categorie Sport ? */
SELECT j.nom  
FROM Jeu AS j, Studio AS s, Categorie AS c 
WHERE j.idstudio = s.id AND j.idcategorie = c.id AND s.nom = 'EA' AND c.nomcategorie = 'Sport';
/* req2 : Auto jointure :  pour chaque client, donnez un autre client qui a le même nom que lui*/
SELECT c1.nom, c1.prenom, c2.nom, c2.prenom 
FROM Compte_client AS c1, Compte_client AS c2 
WHERE c1.id != c2.id and c1.nom = c2.nom;
/* req3 : Sous requête correlé : Pour chaque jeu dont le prix a été supérieur à 80 euros a un moment, donnez son nom et sa catégorie */
SELECT DISTINCT j.nom,c.nomcategorie 
FROM Jeu AS j, Categorie as c  
WHERE j.id IN 
    (SELECT v.idjeu  
    FROM Version_jeu as v, Prix as pr 
    WHERE j.id = v.idjeu AND pr.idplateform = v.idplateform AND pr.idjeu = v.idjeu AND pr.prix >= 80 ) 
          AND j.idcategorie = c.id;
/* req4 : Sous requête dans le FROM : Tout les compte client qui ont au moin 22 ans */
SELECT c.id, c.nom, c.prenom 
FROM ( SELECT * FROM Compte_client WHERE age > 22) as  c;
/* req5 : Sous requette dans le WHERE :
 Donnez les versions de jeux  qui n'ont jamais était commandé 
 (une version est la combinaison d'un jeu et de la plate-forme sur laquelle il tourne)*/
SELECT j.nom, p.nomplatform
FROM Jeu AS j, version_jeu AS v, plateforme AS p
WHERE (v.idjeu, v.idplateform) NOT IN ( SELECT DISTINCT jc.idjeu, jc.idplateform FROM jeu_commande AS jc ) 
AND v.idjeu = j.id AND v.idplateform = p.id
ORDER BY p.nomplatform;
/* req 6 : Deux agrégats nécessitant GROUP BY et HAVING : Donnez pour chaque studio dont l'âge de restriction maximale (de leurs jeux) est strictement 
inférieur à 10 ans, le nombre de jeux qu'ils ont développés */
SELECT s.nom, COUNT(j.id) AS nombre 
FROM Jeu AS j, Studio AS s 
WHERE j.idstudio=s.id  
GROUP BY s.id 
HAVING MAX(j.age_restriction)<10 ;
/* req 7 : Requete impliquant le calcul de deux agrégats :  
Donnez pour chaque catégorie de jeu, la moyenne des prix maximum insérés dans le temps */
SELECT c.nomcategorie,cast( AVG(Maxp.maxprix) AS DECIMAL(10,3))   as moyenne_des_prix_max
FROM Jeu as j, Categorie as c,
    (SELECT p.idjeu, MAX(p.prix) as maxprix 
    FROM prix as p 
    GROUP BY (p.idjeu) )  
    as Maxp 
WHERE j.id = Maxp.idjeu AND j.idcategorie = c.id
GROUP BY c.id
;
/* req 8v1 : Condition de totalité  avec requête corrélée : Les jeux sortie sur toute les plateformes */
SELECT  distinct j.nom  FROM Jeu AS j
WHERE
   NOT EXISTS 
   ( Select * 
     FROM Plateforme as p 
     WHERE p.id NOT IN ( SELECT v.idplateform FROM Version_jeu as v WHERE j.id = v.idjeu  )) ;
/* req 8v2 : Condition de totalité  avec agrégation : Les jeux sortie sur toute les plateformes */
SELECT v.idjeu,j.nom 
FROM Jeu AS j, Version_jeu AS v
WHERE  v.idjeu = j.id  
GROUP BY v.idjeu, j.nom
HAVING COUNT(DISTINCT v.idplateform ) = (SELECT COUNT(p1.id) FROM Plateforme AS p1)
;
/* req 9 : Jointure externe en LEFT JOIN : 
Donnez pour chaque client (nom, prénom et id) les commandes qu'ils ont passé (idcommande et date d'insertion, et NULL si le client na rien commandé avec son compte)  */
SELECT c.nom, c.prenom, c.id as idclient_table_client, com.idclient as idclient_table_commande, com.id as idcommande, com.dateinsertion as date_insertion_de_commande
FROM Compte_client AS c LEFT JOIN Commande as com ON c.id = com.idclient 
ORDER BY c.nom, c.prenom
;
/* req 10v1 : Gestion des NULL avec une simple jointure :
 Les noms d'entreprises qui ont développé des jeux et construit des consoles */
SELECT DISTINCT s.nom 
FROM Plateforme AS p, Studio AS s 
WHERE p.marque = s.nom
;
/* req 10v2 : Gestion des NULL avec INTERSECT :
 Les noms d'entreprises qui ont développé des jeux et construit des consoles */
(SELECT p.marque FROM Plateforme as p) 
INTERSECT ALL 
(SELECT s.nom FROM Studio AS s)
;
/* req 11v2 : Donnez la liste des 10 jeux les plus vendus avec le nombre de commandes par jeux */
SELECT j.nom, jec.nombre_commandes 
FROM jeu AS j, (SELECT jc.idjeu, COUNT(jc.idjeu) AS nombre_commandes 
                FROM jeu_commande AS jc 
                GROUP BY jc.idjeu) AS jec 
WHERE jec.idjeu = j.id 
ORDER BY jec.nombre_commandes DESC LIMIT 10;
/* req 12 : Les versions de jeux dont les prix évoluent mais ne diminuent jamais */
SELECT DISTINCT j.id AS idjeu, j.nom AS nom_jeu, v.idplateform , p.nomplatform
FROM Jeu AS j, version_jeu as v, plateforme as p
WHERE (v.idjeu, v.idplateform) IN
     (SELECT idjeu, idplateform 
     FROM Prix 
     GROUP BY (idjeu, idplateform )
     HAVING COUNT(idjeu)>1) AND j.id = v.idjeu AND p.id = v.idplateform
EXCEPT 
SELECT DISTINCT p1.idjeu, j.nom, p.id, p.nomplatform
FROM Plateforme as p, Jeu AS j, prix AS p1 JOIN prix AS p2 ON  (p1.idjeu = p2.idjeu 
     AND p2.idplateform =p1.idplateform)
WHERE p1.date_insertion < p2.date_insertion AND (p1.prix > p2.prix ) AND p1.idjeu = j.id 
     AND p.id = p1.idplateform
;
/* req 13 : Donnez pour chaque jeux, la moyenne des notes qu'il a recu */
SELECT j.nom,CAST( AVG(jm.note) AS DECIMAL(10,2)) 
FROM (SELECT v.idjeu AS idjeu , a.note AS note 
      FROM Jeu AS j, version_jeu AS v, avis AS a 
      WHERE v.idjeu = a.idjeu AND a.idplateform = v.idplateform) AS jm 
      JOIN jeu AS j ON j.id = jm.idjeu 
GROUP BY j.id
;
/* req 14 : Donnez  le nombre et la moyenne d'âge des clients par catégorie de jeux */
SELECT c.nomcategorie, COUNT(cl.id) AS nombre_jeu, CAST(AVG(cl.age) AS DECIMAL(10,2)) AS moyenne_age
FROM  Jeu AS j JOIN Categorie AS c ON (c.id = j.idcategorie) 
    JOIN Jeu_commande AS jc ON ( j.id = jc.idjeu ) JOIN commande AS com 
    ON (jc.idcommande = com.id) JOIN Compte_client AS cl ON (com.idclient = cl.id)
GROUP BY c.id
;
/* req 15 : Donnez pour chaque studio leur premier et dernier jeu sortie ? */
WITH lastfirstgame(id, datemin, datemax ) AS (
    SELECT s.id, MIN(v.date_de_sortie) AS datemin, MAX(v.date_de_sortie) AS datemax
    FROM Jeu as j, Studio AS s, version_jeu AS v
    WHERE j.idstudio = s.id AND j.id = v.idjeu
    GROUP BY s.id,s.nom
)
SELECT s.nom AS nom_studio, j1.nom AS nom_premier_jeu, v1.date_de_sortie AS 
    date_de_sortie_premier_jeu, j2.nom AS nom_dernier_jeu , v2.date_de_sortie AS date_de_sortie_dernier_jeu
FROM Studio AS s, Jeu AS j1, Jeu AS j2, version_jeu AS v1, version_jeu AS v2, lastfirstgame AS l
WHERE l.id = s.id AND l.datemin = v1.date_de_sortie AND l.datemax = v2.date_de_sortie
      AND s.id = j1.idstudio AND s.id = j2.idstudio AND j1.id = v1.idjeu AND j2.id = v2.idjeu
;
/* req 16 : Donnez pour chaque client, les jeux qu'il a le mieux notés  */
WITH maxnote (idcomptecli, notemax) AS (
SELECT a.idcomptecli, max(a.note) AS notemax 
FROM Avis AS a
GROUP BY a.idcomptecli)
SELECT c.id,c.prenom,c.nom, j.nom, av.note
FROM Jeu AS j, Compte_client AS c, maxnote AS a, avis AS av
WHERE j.id = av.idjeu AND  av.note = a.notemax AND av.idcomptecli = a.idcomptecli 
      AND av.idcomptecli = c.id
ORDER BY c.id
;
/* req 17 : Donnez pour chaque plateforme, le nombre et prix moyen  de ses jeux 
( par rapport à la date d'ajourd'hui ) */
WITH mod(idjeu, idplateform, dateinsert) AS (
  SELECT p.idjeu, p.idplateform, MAX(p.date_insertion) AS dateinsert
  FROM prix AS p 
  WHERE current_date >p.date_insertion
  GROUP BY p.idjeu, p.idplateform ORDER BY p.idplateform, p.idjeu
)

SELECT pl.nomplatform, COUNT(DISTINCT pri.idjeu) AS nombre_de_jeux, 
        CAST( AVG (pri.prix) AS DECIMAL(10,2))  AS prix_moyen_des_jeux
FROM plateforme AS pl, prix AS pri, mod
WHERE  mod.dateinsert = pri.date_insertion AND mod.idjeu = pri.idjeu 
AND mod.idplateform = pri.idplateform AND mod.idplateform = pl.id
GROUP BY pl.nomplatform, pri.idplateform
;
/* req 18 : Donnez pour chaque commande, la somme totale déboursé pour celle-ci 
(ainsi que le client qui lui est associé) */
WITH dernier_prix_commande (idjeu, idplateform, lastdate) AS 
(SELECT p.idjeu, P.idplateform, MAX(p.date_insertion) AS lastdate
FROM commande AS c, jeu_commande AS jc, prix AS p 
where c.dateinsertion > p.date_insertion AND p.idjeu = jc.idjeu 
      AND p.idplateform = jc.idplateform AND jc.idcommande = c.id
GROUP BY (p.idjeu, p.idplateform)
ORDER BY (p.idplateform,p.idjeu))

SELECT c.id AS id_commande, cl.nom,cl.prenom,SUM(pr.prix) AS somme FROM commande AS c, 
      jeu_commande AS jc, dernier_prix_commande AS p , prix AS pr, compte_client AS cl
WHERE p.idjeu = jc.idjeu AND p.idplateform = jc.idplateform AND jc.idcommande = c.id 
      AND pr.idjeu = p.idjeu AND pr.idplateform=p.idplateform 
      AND pr.date_insertion=p.lastdate AND cl.id = c.idclient
GROUP BY c.id,cl.id
;
/* req 19 : Donnez pour chaque version de jeu, la difference de prix en pourcentage
 entre chaque nouvelle insertion/modification de prix dans le temps */
WITH datecoupleprix(idjeu, idplateform, premdate, dexdate ) AS 
        (SELECT p1.idjeu, p1.idplateform, p1.date_insertion AS premdate,
                 MIN(p2.date_insertion) AS dexdate
        FROM prix AS p1, prix AS p2 WHERE p1.idjeu = p2.idjeu AND p1.idplateform = p2.idplateform 
        AND p1.date_insertion < p2.date_insertion 
        GROUP BY (p1.idjeu, p1.idplateform, p1.date_insertion) 
        ORDER BY (p1.idjeu, p1.idplateform, p1.date_insertion))
SELECT j.nom, pl.nomplatform, dp.premdate AS premiere_date, dp.dexdate AS deuxieme_date, p1.prix AS prix1,
         p2.prix AS prix2, CONCAT (((p1.prix-p2.prix)*100/p1.prix), '%') as pourcentage_diff
FROM datecoupleprix AS dp,jeu AS j, prix AS p1, prix AS p2, plateforme AS pl
WHERE dp.idjeu = p1.idjeu AND dp.idjeu = p2.idjeu AND dp.idplateform = p1.idplateform 
      AND dp.idplateform = p2.idplateform AND p1.date_insertion = dp.premdate 
      AND p2.date_insertion = dp.dexdate AND j.id = dp.idjeu AND pl.id = dp.idplateform
;
/* req 20 : Donnez pour chaque mois d'activité, le revenu (mensuel) 
en calculant la somme des coups et des bénéfices */
WITH moisnum(mois) AS(
SELECT DISTINCT EXTRACT(MONTH FROM c.dateinsertion) AS mois
FROM commande AS c),

dernieredate (idjeu,idplateform,dateprix) AS(
SELECT pr.idjeu, pr.idplateform, MAX(pr.date_insertion) AS dateprix
FROM Jeu_commande AS jc, prix AS pr, commande AS c
WHERE jc.idcommande = c.id AND c.dateinsertion >= pr.date_insertion 
AND jc.idjeu = pr.idjeu AND jc.idplateform = pr.idplateform
GROUP BY pr.idjeu, pr.idplateform)

SELECT m.mois, SUM(pr.prix - v.prix_acquisition) AS revenu
FROM prix AS pr, moisnum AS m, dernieredate AS dt , version_jeu AS v, 
     commande AS com, jeu_commande AS jc1
WHERE dt.idjeu = pr.idjeu AND dt.idplateform = pr.idplateform 
      AND dt.dateprix = pr.date_insertion AND v.idjeu = dt.idjeu 
      AND v.idplateform = dt.idplateform AND jc1.idjeu = dt.idjeu 
      AND jc1.idplateform = dt.idplateform AND jc1.idcommande = com.id 
      AND EXTRACT(MONTH FROM com.dateinsertion) = m.mois
GROUP BY m.mois
;