CREATE SCHEMA projetbdd;
SET search_path TO projetbdd;

CREATE TABLE Studio (
    id INTEGER PRIMARY KEY,
    nom TEXT,
    Localisation TEXT
);
CREATE TABLE Categorie (
    id INTEGER PRIMARY KEY,
    nomcategorie TEXT UNIQUE
);
CREATE TABLE Plateforme (
    id INTEGER PRIMARY KEY,
    nomplatform TEXT NOT NULL UNIQUE,
    marque TEXT
);
CREATE TABLE Jeu (
    id INTEGER PRIMARY KEY,
    nom TEXT NOT NULL,
    age_restriction INTEGER NOT NULL,
    idstudio INTEGER NOT NULL,
    idcategorie INTEGER NOT NULL,
    FOREIGN KEY (idcategorie) REFERENCES categorie(id),
    FOREIGN KEY (idstudio)  REFERENCES studio(id)
);
CREATE TABLE Version_jeu (
    idjeu INTEGER NOT NULL,
    idplateform INTEGER NOT NULL,
    date_de_sortie DATE,
    prix_acquisition INTEGER NOT NULL,
    FOREIGN KEY (idjeu) REFERENCES jeu(id),
    FOREIGN KEY (idplateform) REFERENCES plateforme(id),
    PRIMARY KEY (idplateform, idjeu)
);
CREATE TABLE Prix (
    idjeu INTEGER NOT NULL,
    idplateform INTEGER NOT NULL,
    date_insertion DATE NOT NULL,
    prix INTEGER NOT NULL,  
    FOREIGN KEY (idplateform, idjeu) REFERENCES Version_jeu(idplateform, idjeu),
    PRIMARY KEY (idplateform, idjeu, date_insertion),
    CHECK (prix>=0)
);
CREATE TABLE Compte_client (
    id INTEGER PRIMARY KEY,
    prenom TEXT,
    nom TEXT,  
    mail TEXT NOT NULL,
    mdp  TEXT NOT NULL,
    age INTEGER NOT NULL,
    adresse TEXT NOT NULL,
    CHECK (age>=15)
);
CREATE TABLE Commande (
    id INTEGER PRIMARY KEY,
    idclient INTEGER NOT NULL,
    dateinsertion DATE NOT NULL,
    FOREIGN KEY (idclient) REFERENCES compte_client(id)
);
CREATE TABLE Avis (
    idcomptecli INTEGER NOT NULL,
    idjeu INTEGER NOT NULL,
    idplateform INTEGER NOT NULL,
    note INTEGER NOT NULL,
    avis TEXT,
    date_insertion DATE NOT NULL,
    date_livraison DATE NOT NULL,
    FOREIGN KEY (idjeu,idplateform) REFERENCES version_jeu(idjeu,idplateform),
    FOREIGN KEY (idcomptecli) REFERENCES Compte_client(id),
    PRIMARY KEY (idplateform, idjeu, idcomptecli),
    CHECK (note>=0  AND note <=20),
    CHECK (date_insertion >= date_livraison)
);
CREATE TABLE Remboursement (
    idcommande INTEGER NOT NULL,
    idjeu INTEGER NOT NULL, 
    idplateform INTEGER NOT NULL, 
    date_de_remb DATE NOT NULL,
    FOREIGN KEY (idplateform,idjeu) REFERENCES version_jeu(idplateform,idjeu),
    FOREIGN KEY (idcommande) REFERENCES commande(id),
    PRIMARY KEY (idplateform, idjeu, idcommande)
);
CREATE TABLE Jeu_commande (
    idcommande INTEGER NOT NULL,
    idjeu INTEGER NOT NULL, 
    idplateform INTEGER NOT NULL,   
    nombre INTEGER NOT NULL,
    FOREIGN KEY (idplateform,idjeu) REFERENCES version_jeu(idplateform,idjeu),
    FOREIGN KEY (idcommande) REFERENCES commande(id),
    PRIMARY KEY (idplateform, idjeu, idcommande),
    CHECK (nombre>=1)
);
CREATE TABLE Jeu_en_livraison(
    idcommande INTEGER NOT NULL,
    idjeu INTEGER NOT NULL, 
    idplateform INTEGER NOT NULL,   
    nombre INTEGER NOT NULL,
    date_expedition DATE NOT NULL,
    date_estimee_livraison DATE NOT NULL,
    FOREIGN KEY (idplateform,idjeu,idcommande) REFERENCES jeu_commande(idplateform,idjeu,idcommande),    
    PRIMARY KEY (idplateform, idjeu, idcommande),
    CHECK (nombre>=1)
);
CREATE TABLE Jeu_en_preparation (
    idcommande INTEGER NOT NULL,
    idjeu INTEGER NOT NULL, 
    idplateform INTEGER NOT NULL,   
    nombre INTEGER NOT NULL, 
    emplacement_stock TEXT NOT NULL,
    FOREIGN KEY (idplateform,idjeu,idcommande) REFERENCES jeu_commande(idplateform,idjeu,idcommande),    
    PRIMARY KEY (idplateform, idjeu, idcommande),
    CHECK (nombre>=1)
);
CREATE TABLE Jeu_en_attente (
    idcommande INTEGER NOT NULL,
    idjeu INTEGER NOT NULL,
    idplateform INTEGER NOT NULL, 
    nombre INTEGER NOT NULL,
    date_livraison_stock DATE NOT NULL,
    FOREIGN KEY (idplateform,idjeu,idcommande) REFERENCES jeu_commande(idplateform,idjeu,idcommande),    
    PRIMARY KEY (idplateform, idjeu, idcommande),
    CHECK (nombre>=1)
);
CREATE TABLE Jeu_livré(
    idcommande INTEGER NOT NULL,
    idjeu INTEGER NOT NULL, 
    idplateform INTEGER NOT NULL,   
    nombre INTEGER NOT NULL,
    date_de_livraison DATE NOT NULL,
    FOREIGN KEY (idplateform,idjeu,idcommande) REFERENCES jeu_commande(idplateform,idjeu,idcommande),    
    PRIMARY KEY (idplateform, idjeu, idcommande),
    CHECK (nombre>=1)
);
CREATE TABLE Panier (
    id INTEGER,
    idjeu INTEGER NOT NULL,
    idplateform INTEGER NOT NULL,
    idcli INTEGER,
    nombre INTEGER,
    FOREIGN KEY (idjeu) REFERENCES jeu(id),
    FOREIGN KEY (idplateform) REFERENCES plateforme(id),
    FOREIGN KEY (idcli) REFERENCES compte_client(id),
    PRIMARY KEY (id,idjeu,idplateform)
);
